import React from 'react'
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createDrawerNavigator } from '@react-navigation/drawer'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'

import About from './Tugas1/AboutScreen'
import LogIn from './Tugas1/LoginScreen'
import Bahasa from './Tugas14/BahasaPemrograman'
import Framework from './Tugas14/Framework'
import Teknologi from './Tugas14/Teknologi'
import Yours from './Tugas14/YourSkills'
import YourBahasa from './Tugas14/Yours/YourBahasa'
import YourFramework from './Tugas14/Yours/YourFramework'
import YourTech from './Tugas14/Yours/YourTech'

const Stack = createStackNavigator()
const Drawer = createDrawerNavigator()
const Tabs = createBottomTabNavigator()

const DrawerScreen = () => {
  return (
    <Drawer.Navigator>
      <Drawer.Screen name="Profile" component={Yours}/>
      <Drawer.Screen name="About Developer" component={About}/>
    </Drawer.Navigator>
  )
}
const TabsScreen = () => (
  <Tabs.Navigator>
    <Tabs.Screen name="Programming" component={Bahasa}/>
    <Tabs.Screen name="Framework" component={Framework}/>
    <Tabs.Screen name="Technology" component={Teknologi}/>
  </Tabs.Navigator>
)

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="LogIn" component={LogIn} options={{ headerShown: false }}/>
        <Stack.Screen name="DrawerScreen" component={DrawerScreen} options={{ headerShown: false }}/>
        <Stack.Screen name="TabScreen" component={TabsScreen} options={{ headerShown: false }}/>
        <Stack.Screen name="YourBahasa" component={YourBahasa} options={{ headerShown: false }}/>
        <Stack.Screen name="YourFramework" component={YourFramework} options={{ headerShown: false }}/>     
        <Stack.Screen name="YourTech" component={YourTech} options={{ headerShown: false }}/>
      </Stack.Navigator>
    </NavigationContainer>
  )
}