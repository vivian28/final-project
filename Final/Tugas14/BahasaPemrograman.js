import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, FlatList } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'

export default class Skill extends React.Component {
    state = {
        data: []
    }
    componentDidMount() {
        this.fetchData()
    }
    fetchData = async () => {
        const response = await fetch('https://final-project-8993f.firebaseio.com/users.json')
        const json = await response.json()
        this.setState({data: json})
    }

    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                <View style={styles.icon}>
                    <TouchableOpacity onPress={() => navigate('LogIn')}>
                        <Icon name="arrow-back" size={40}/>
                    </TouchableOpacity>   
                    <View style={styles.vector}>
                        <Text style={styles.title}>Programming</Text>
                    </View>
                </View>
                <FlatList
                    data={this.state.data}
                    renderItem={({ item }) =>
                        <View style={styles.viewList}>
                            <View>
                                <Image source={{ uri: `${item.pics}` }} style={styles.Image} />
                            </View>
                            <View style={{ alignItems: 'flex-start', marginLeft: 20}}>
                                <Text style={styles.textItemLogin}> {item.name}</Text>
                                <View style={{flexDirection: 'row'}}>
                                    <Text style={{fontSize: 18}}>{item.bahasa}</Text>
                                    <Text style={{fontSize: 18, marginLeft: 10}}>{item.levelb}</Text>
                                </View>
                            </View>
                        </View>
                    }
                    keyExtractor={({ id }, index) => index}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: ('#FF8080')
    },
    icon: {
        marginTop: 30,
        marginLeft: 25,
        flexDirection: 'row',
        marginBottom: 40
    },
    title: {
        fontSize: 21,
        fontFamily: 'sans-serif-medium'
    },
    vector: {
        marginLeft: 30,
        alignItems: 'center',
        justifyContent: 'center',
        width: 250,
        height: 50,
        borderRadius: 100,
        backgroundColor: 'white'
    },
    viewList: {
        height: 130,
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: '#DDD',
        alignItems: 'center'
    },
    Image: {
        width: 90,
        height: 90,
        borderRadius: 80,
        marginLeft: 20
    },
    textItemLogin: {
        fontWeight: 'bold',
        textTransform: 'capitalize',
        fontSize: 20
    }
})