import React, { Component } from 'react'
import { View, Text, StyleSheet } from 'react-native'

export default class Title extends Component {
  render() {
    const { children } = this.props

    return (
      <View style={{backgroundColor: '#00B749', padding: 25}}>
        <View style={styles.header}>
          <Text style={styles.title}>{ children }</Text>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: 'white',
    alignSelf: 'center',
    justifyContent: 'center',
    width: 250,
    height: 50,
    borderRadius: 100,
  },
  title: {
    textAlign: 'center',
    color: 'black',
    fontSize: 28
  },
})