import React, { Component } from 'react'
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native'

export default class List extends Component {
  renderItem = (text, i) => {
    const { onPressItem } = this.props

    return (
      <TouchableOpacity key={i} style={styles.item} onPress={() => onPressItem(i)}>
        <Text style={styles.teks}>{text}</Text>
      </TouchableOpacity>
    )
  }

  render() {
    const { list } = this.props
    return <View>{list.map(this.renderItem)}</View>
  }
}

const styles = StyleSheet.create({
  item: {
    backgroundColor: '#00B749',
    marginBottom: 5,
    padding: 30,
  },
  teks: {
    alignSelf: 'center',
    fontSize: 30,
    fontWeight: '500'
  }
})