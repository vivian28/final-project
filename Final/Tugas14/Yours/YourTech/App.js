import React from 'react'
import { View, Text } from 'react-native'
import { connect } from 'react-redux'

import { actionCreators } from './todoListRedux'
import List from './List'
import Input from './Input'
import Title from './Title'

const mapStateToProps = (state) => ({
  input: state.input,
})

class App extends React.Component {

  onAddTodo = (text) => {
    const { dispatch } = this.props
    dispatch(actionCreators.add(text))
  }

  onRemoveTodo = (index) => {
    const { dispatch } = this.props
    dispatch(actionCreators.remove(index))
  }

  render() {
    const { input } = this.props

    return (
      <View>
        <Title>
            Technology Skill
        </Title>
        <Input
          placeholder={'Write Your Skill and Level Here'}
          onSubmitEditing={this.onAddTodo}
        />
        <Text>Note: press your skill if you want to delete your data</Text>
        <List
          list={input}
          onPressItem={this.onRemoveTodo}
        />
      </View>
    )
  }
}

export default connect(mapStateToProps)(App)