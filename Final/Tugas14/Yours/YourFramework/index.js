import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import { Provider } from 'react-redux'

import App from './App'

import store from './store'

export default function AppWithStore({ navigation: { goBack } }) {
  return (
    <View>
      <Provider store={store}>
        <App/>
      </Provider>
      <View style={{marginTop: 20, alignSelf: 'center'}}>
        <TouchableOpacity onPress={() => goBack()}>
          <View style={styles.button}>
            <Text style={styles.text2}>Back</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  button: {
    backgroundColor: '#0C1B6C',
    height: 45,
    width: 100,
    borderRadius: 8
  },
  text2: {
    fontSize: 25,
    color: 'white',
    alignSelf: 'center'
},  
})