export const types = {
    ADD: 'ADD',
    REMOVE: 'REMOVE',
  }

  export const actionCreators = {
    add: item => {
      return { type: types.ADD, payload: item }
    },
    remove: index => {
      return { type: types.REMOVE, payload: index }
    },
  }
  
  const initialState = {
    input: [],
  }
  export const reducer = (state = initialState, action) => {
    const { input } = state
    const { type, payload } = action
  
    switch (type) {
      case types.ADD: {
        return {
          ...state,
          input: [payload, ...input],
        }
      }
      case types.REMOVE: {
        return {
          ...state,
          input: input.filter((write, i) => i !== payload),
        }
      }
    }
  
    return state
}