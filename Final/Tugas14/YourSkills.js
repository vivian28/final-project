import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, ImageBackground } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'

export default class Skill extends React.Component {
    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                <View style={styles.vector}>
                    <Text style={styles.title}>Profile</Text>
                </View>
                <View style={styles.pp}>
                    <Image source={require('./photo/people.png')} style={{width: 110, height: 110}}/>
                    <View style={styles.acc}>
                        <Text style={{fontSize: 30, fontFamily: 'serif'}}>Rayn</Text>
                    </View>
                </View>
                <Text style={styles.subtitle}>Which Skill Do You Want to Update?</Text>
                <View style={{alignItems: 'center', marginTop: 30}}>
                        <TouchableOpacity onPress={() => navigate("YourBahasa")}>
                            <View style={styles.pembatas}>
                                <ImageBackground source={require('./photo/bahasapemrograman.jpeg')} style={styles.foto}>
                                    <Text style={styles.text3}>Programming</Text>
                                </ImageBackground>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => navigate("YourFramework")}>
                            <View style={styles.pembatas}>
                                <ImageBackground source={require('./photo/framework.jpg')} style={styles.foto}>
                                    <Text style={styles.text3}>Framework</Text>
                                </ImageBackground>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => navigate("YourTech")}>
                            <View style={styles.pembatas}>
                                <ImageBackground source={require('./photo/techyes2.png')} style={styles.foto}>
                                    <Text style={styles.text3}>Technology</Text>
                                </ImageBackground>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('LogIn')}>
                            <View style={styles.button}>
                                <Text style={styles.text2}>Log Out</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: ('#FFB800')
    },
    title: {
        fontSize: 21,
        fontFamily: 'sans-serif-medium',
        alignSelf: 'center'
    },
    vector: {
        marginTop: 50,
        alignSelf: 'center',
        justifyContent: 'center',
        width: 250,
        height: 50,
        borderRadius: 100,
        backgroundColor: 'white'
    },
    pp: {
        marginLeft: 40,
        marginTop: 40,
        flexDirection: 'row'
    },
    acc: {
        alignSelf: 'center',
        marginLeft: 20
    },
    subtitle: {
        alignSelf: 'center',
        fontSize: 20,
        marginTop: 20,
        marginBottom: -30
    },
    pembatas: {
        marginVertical: 5
    },
    foto: {
        height: 120,
        width: 350,
        marginTop: 20,
        borderRadius: 30
    },
    text3: {
        fontWeight: 'bold',
        color: 'white',
        position: 'absolute',
        bottom: 5,
        right: 5,
        fontSize: 20,
        color: 'black',
        backgroundColor: 'white'
    },
    button: {
        backgroundColor: '#0C1B6C',
        marginTop: 20,
        height: 45,
        width: 120,
        borderRadius: 8,
        alignSelf: 'center'
    },
    text2: {
        fontSize: 25,
        color: 'white',
        alignSelf: 'center'
    }
})